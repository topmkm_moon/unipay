class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :event, index: true, foreign_key: true
      t.float :nprice
      t.references :debtor, index: true, foreign_key: true
      t.references :creditor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
