json.array!(@events) do |event|
  json.extract! event, :id, :user_id, :title, :tprice
  json.url event_url(event, format: :json)
end
